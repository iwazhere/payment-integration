﻿<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<?php 
/*
*Template Name: Hosted Payment
*
*/
?>
<?php
	//------------------------------------------------------------------------------------------------------------------------
	// OVERVIEW
	// 
	// Collection information for and process the results of online payments. Redirect process flows website.
	// 
	// 
	//------------------------------------------------------------------------------------------------------------------------
	
	// TODO: Add your sandbox (test) environment credentials...
	$username = 'xxxx';
	$password = 'xxxxxxxxxx';
	//$baseUrl = 'https://testpayments.integrapay.com.au/';
	// orig $baseUrl = 'https://sandbox.paymentsapi.io/basic/PayLinkService.svc';
	$baseUrl = 'https://sandbox.paymentsapi.io/basic/PayLinkService.svc?WSDL';
	//$baseUrl = 'https://testpayments.integrapay.com.au/RTP/Payment.aspx?webPageToken'

	// TODO: Add your live (production) environment credentials...
	// $username = '';
	// $password = '';
	// $baseUrl = 'https://payments.integrapay.com.au/';

	// TODO: Change the page title for your website...
	$title = 'IntegraPay Hosted Payment Services';

	$apiUrl = $baseUrl.'API/API.ashx';
	$homeUrl = 'http://www.integrapay.com.au/';
	$paymentUrl = $baseUrl.'RTP/Payment.aspx?webPageToken=';
	$jsUrl = $baseUrl.'JS/';

	$page = isset($_GET['page']) ? $_GET['page'] : 'Setup';
	echo $page;
	$redirectUrl = NULL;

	// To support all possible process flows, when the 'page' to be shown is the Payment page then
	// issue the PreHostedRealTimePayment request prior to rendering the page. This allows us to
	// redirect to a hosted payment page when required, rather than showing our own payment page.
	if ($page == 'Payment')
	{
		$flow = $_POST['flow'];
		$transactionID = $_POST['transactionID'];
		$transactionAmountInCents = $_POST['transactionAmountInCents'];

		if (isset($flow) && isset($transactionID) && isset($transactionAmountInCents))
		{
			$request = PreHostedRealTimePaymentRequest($username, $password, $homeUrl);
			$response = SendHttpRequest($apiUrl, "POST", $request);
			$xmlDoc = new SimpleXMLElement($response);
			if ($xmlDoc->result == 'OK' && UsingHostedPageRedirect())
			{
				$redirectUrl = $paymentUrl.$xmlDoc->webPageToken;
			}
		}
	}

	if (isset($redirectUrl))
	{
		header('Location: '.$redirectUrl);
	}
	else // not redirecting...
	{
?>
		<head>
			<meta charset="utf-8" />
			<title><?php echo $title; ?></title>

			<!-- Bootstrap/JQuery Stylesheets/JavaScript -->
			<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
			<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
			<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
			<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

			<?php echo ($page == 'Payment' && UsingJavaScriptPayments()) ? '<script src="'.$jsUrl.'"></script>' : ''; ?>
		</head>
		<body>
			<div class="container">
<?php
				// If any PHP variables required by this script are missing or invalid, then display an error.
				if (!isset($username) || empty($username) ||
					!isset($password) || empty($password) ||
					!isset($baseUrl) || empty($baseUrl))
				{
?>
					<div class="alert alert-danger">
						<h4>Missing or invalid PHP variables</h4>
						<strong>The following variables must be set in the PHP script file:</strong>
						<ul>
							<li>$username</li>
							<li>$password</li>
							<li>$baseUrl</li>
						</ul>
					</div>
<?php
				}
				else // required variables are valid; render the 'page' to be shown...
				{
?>
					<h3><?php echo $page; ?> Page</h3>
					<hr/>
<?php
					if ($page == 'Setup')
					{
						//----------------------------------------------------------------------------------------------------
						// SETUP PAGE
						// 
						// This section represents the page(s) of your website that collect information prior to an online
						// payment occurring. This includes a unique transaction ID for each payment attempt (regardless of
						// whether previous attempts failed), the payment amount and description, and various payer details.
						//----------------------------------------------------------------------------------------------------

						// TODO: Change transaction ID creation for your website...
						$transactionID = date('YmdHis');

						if (IsLocalUrl(PageUrl('Result')))
						{
?>
							<div class="alert alert-warning">
								<h4>You are running this website from a URL that is local to your computer</h4>
								<div>
									Payments using the Hosted Page Redirect and Transparent Redirect methods are redirected
									from your website to another hosted by IntegraPay during processing. After the payment
									attempt is processed, IntegraPay redirects back to your website. URLs that are local to
									your computer cannot be reached from another machine, therefore this sample PHP script
									will redirect back to <a href="<?php echo $homeUrl; ?>"><?php echo $homeUrl; ?></a>.
								</div>
							</div>
<?php
						}
?>
						<script type="text/javascript">
							function toggleShowButton(obj)
							{
								obj.innerHTML = (obj.innerHTML === "Show More" ? "Show Less" : "Show More");
								return false;
							}
						</script>
						<form class="form-horizontal" method="POST" action="<?php echo PageUrl('Payment'); ?>">
							<div class="panel panel-default">
								<div class="panel-heading">Sample Configuration</div>
								<div class="panel-body">
									<div class="form-group">
										<label class="control-label col-sm-2">API Process Flow</label>
										<div class="col-sm-3">
											<div>
												<label><input type="radio" name="flow" value="JavaScriptPayments" checked /> JavaScript Payments</label>
												<label><input type="radio" name="flow" value="HostedPageRedirect" /> Hosted Page Redirect</label>
												<label><input type="radio" name="flow" value="TransparentRedirect" /> Transparent Redirect</label>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">Minimum Required Information</div>
								<div class="panel-body">
									<div class="form-group">
										<label for="transactionID" class="control-label col-sm-2">Transaction ID</label>
										<div class="col-sm-3">
											<input id="transactionID" class="form-control" maxlength="100" name="transactionID" placeholder="unique for each attempt" type="text" value="<?php echo $transactionID; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label for="transactionAmountInCents" class="control-label col-sm-2">Amount in Cents</label>
										<div class="col-sm-2">
											<input id="transactionAmountInCents" autofocus class="form-control" name="transactionAmountInCents" placeholder="e.g. 1000 for $10.00" type="number" />
										</div>
									</div>
									<div class="form-group">
										<label for="transactionDescription" class="control-label col-sm-2">Description</label>
										<div class="col-sm-5">
											<input id="transactionDescription" class="form-control" maxlength="200" name="transactionDescription" placeholder="e.g. Payment for goods and services rendered" type="text" />
										</div>
									</div>
								</div>
							</div>
							<div id="optional" class="collapse">

								<div class="panel panel-default">
									<div class="panel-heading">Payer Information</div>
									<div class="panel-body">
										<div class="form-group">
											<label for="payerUniqueID" class="control-label col-sm-2">Unique ID</label>
											<div class="col-sm-3">
												<input id="payerUniqueID" class="form-control" maxlength="100" value="<?php echo rand(1,10000);?>" name="payerUniqueID" placeholder="unique for each payer" type="text" />
											</div>
										</div>
										<div class="form-group">
											<label for="payerFirstName" class="control-label col-sm-2">First Name</label>
											<div class="col-sm-3">
												<input id="payerFirstName" class="form-control" maxlength="50" name="payerFirstName" placeholder="e.g. Jane" type="text" />
											</div>
										</div>
										<div class="form-group">
											<label for="payerLastName" class="control-label col-sm-2">Last Name</label>
											<div class="col-sm-3">
												<input id="payerLastName" class="form-control" maxlength="50" name="payerLastName" placeholder="e.g. Citizen" type="text" />
											</div>
										</div>
										<div class="form-group">
											<label for="payerEmail" class="control-label col-sm-2">Email Address</label>
											<div class="col-sm-4">
												<input id="payerEmail" class="form-control" maxlength="255" name="payerEmail" placeholder="e.g. jane.citizen@example.com" type="email" />
											</div>
										</div>
										<div class="form-group">
											<label for="payerMobile" class="control-label col-sm-2">Mobile Phone</label>
											<div class="col-sm-2">
												<input id="payerMobile" class="form-control" maxlength="50" name="payerMobile" placeholder="e.g. 0412 345 678" type="tel" />
											</div>
										</div>
										<div class="form-group">
											<label for="payerPhone" class="control-label col-sm-2">Other Phone</label>
											<div class="col-sm-2">
												<input id="payerPhone" class="form-control" maxlength="50" name="payerPhone" placeholder="e.g. 07 8765 4321" type="tel" />
											</div>
										</div>
										<div class="form-group">
											<label for="payerAddressStreet" class="control-label col-sm-2">Billing Address</label>
											<div class="col-sm-5">
												<input id="payerAddressStreet" class="form-control" maxlength="255" name="payerAddressStreet" placeholder="e.g. 123 Example Street" type="text" />
											</div>
										</div>
										<div class="form-group">
											<label for="payerAddressSuburb" class="control-label col-sm-2">Suburb / Town</label>
											<div class="col-sm-3">
												<input id="payerAddressSuburb" class="form-control" maxlength="50" name="payerAddressSuburb" placeholder="e.g. Brisbane" type="text" />
											</div>
										</div>
										<div class="form-group">
											<label for="payerAddressState" class="control-label col-sm-2">State</label>
											<div class="col-sm-2">
												<input id="payerAddressState" class="form-control" maxlength="50" name="payerAddressState" placeholder="e.g. QLD" type="text" />
											</div>
										</div>
										<div class="form-group">
											<label for="payerAddressPostCode" class="control-label col-sm-2">Post Code / ZIP</label>
											<div class="col-sm-2">
												<input id="payerAddressPostCode" class="form-control" maxlength="50" name="payerAddressPostCode" placeholder="e.g. 4000" type="text" />
											</div>
										</div>
										<div class="form-group">
											<label for="payerAddressCountry" class="control-label col-sm-2">Country</label>
											<div class="col-sm-3">
												<input id="payerAddressCountry" class="form-control" maxlength="50" name="payerAddressCountry" placeholder="e.g. Australia" type="text" />
											</div>
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading">Other Information</div>
									<div class="panel-body">
										<div class="form-group">
											<label for="auditUsername" class="control-label col-sm-2">Audit Username</label>
											<div class="col-sm-3">
												<input id="auditUsername" class="form-control" name="auditUsername" placeholder="e.g. salesperson-123" type="text" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-3 col-sm-offset-2">
									<button class="btn btn-default" data-target="#optional" data-toggle="collapse" onclick="return toggleShowButton(this);">Show More</button>
									&nbsp;
									<button class="btn btn-primary" type="submit">Submit</button>
								</div>
							</div>
						</form>
<?php
					}
					elseif ($page == 'Payment')
					{
						//----------------------------------------------------------------------------------------------------
						// PAYMENT PAGE
						// 
						// This section represents the page of your website that collects credit card
						// information, which is not required for the Hosted Page Redirect process flow.
						//----------------------------------------------------------------------------------------------------

						if (!isset($flow) || !isset($transactionID) || !isset($transactionAmountInCents))
						{
?>
							<div class="alert alert-danger">
								<h4>Missing or invalid POST variable(s):</h4>
								<ul>
									<?php if (!isset($flow)) echo '<li>flow</li>'."\r\n"; ?>
									<?php if (!isset($transactionID)) echo '<li>transactionID</li>'."\r\n"; ?>
									<?php if (!isset($transactionAmountInCents)) echo '<li>transactionAmountInCents</li>'."\r\n"; ?>
								</ul>
							</div>
<?php
     					}
						else // required POST variables are set...
						{
							if ($xmlDoc->result == 'OK')
							{
?>
								<form class="form-horizontal" data-integrapay="PaymentForm" method="POST" action="<?php echo UsingJavaScriptPayments() ? PageUrl('Result') : $paymentUrl.$xmlDoc->webPageToken ; ?>">
									<input data-integrapay="ApiToken" name="webPageToken" type="hidden" value="<?php echo $xmlDoc->webPageToken; ?>" />
									<h1><?php echo $xmlDoc->webPageToken; ?></h1>
									<?php echo UsingTransparentRedirect() ? '<input name="TransparentRedirect" type="hidden" value="1" />' : ''; ?>
									<div class="panel panel-default">
										<div class="panel-heading">Credit Card Information</div>
										<div class="panel-body">
											<div class="form-group">
												<label for="CreditCardName" class="control-label col-sm-2">Name on Card</label>
												<div class="col-sm-3 input-group">
													<input id="CreditCardName" autofocus class="form-control" data-integrapay="CardName" name="CreditCardName" placeholder="e.g. Jane Citizen" type="text" autocomplete="off" />
												</div>
											</div>
											<div class="form-group">
												<label for="CreditCardNumber" class="control-label col-sm-2">Card Number</label>
												<div class="col-sm-3 input-group">
													<input id="CreditCardNumber" class="form-control" data-integrapay="CardNumber" name="CreditCardNumber" placeholder="e.g. 4242 4242 4242 4242" type="text" autocomplete="off" />
												</div>
											</div>
											<div class="form-group">
												<label for="CreditCardExpiryMonth" class="control-label col-sm-2">Expiry Date</label>
												<div class="col-sm-3 input-group">
													<select id="CreditCardExpiryMonth" class="form-control" data-integrapay="CardExpiryMonth" name="CreditCardExpiryMonth" style="width: 60%">
														<?php WriteMonthOptions(); ?>
													</select>
													<select id="CreditCardExpiryYear" class="form-control" data-integrapay="CardExpiryYear" name="CreditCardExpiryYear" style="width: 40%">
														<?php WriteYearOptions(); ?>
													</select>
												</div>
											</div>
											<div class="form-group">
												<label for="CreditCardSecurityCode" class="control-label col-sm-2">CCV</label>
												<div class="col-sm-3 input-group">
													<input id="CreditCardSecurityCode" class="form-control" data-integrapay="CardCcv" name="CreditCardSecurityCode" placeholder="e.g. 123" style="width: 40%" type="text" autocomplete="off" />
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-offset-2">
													<div class="text-danger" data-integrapay="Errors"></div>
													<div data-integrapay="Processing"></div>
												</div>
											</div>
										</div>
									</div>
<?php
									if (UsingJavaScriptPayments())
									{
?>
										<div class="panel panel-default">
											<div class="panel-heading">JavaScript Payments allow you to include your own &lt;form&gt; inputs</div>
											<div class="panel-body">
												<div class="form-group">
													<label for="sampleInput1" class="control-label col-sm-2">Sample Input 1</label>
													<div class="col-sm-3 input-group">
														<input id="sampleInput1" class="form-control" name="sampleInput1" type="text" />
													</div>
												</div>
												<div class="form-group">
													<label for="sampleInput2" class="control-label col-sm-2">Sample Input 2</label>
													<div class="col-sm-2 input-group">
														<select id="sampleInput2" class="form-control" name="sampleInput2">
															<option value="Option 1">Option 1</option>
															<option value="Option 2">Option 2</option>
															<option value="Option 3">Option 3</option>
														</select>
													</div>
												</div>
											</div>
										</div>
<?php
									}
?>
									<div class="form-group">
										<div class="col-sm-3 col-sm-offset-2">
											<button class="btn btn-default" onclick="location.href='<?php echo PageUrl('Setup'); ?>'" type="button">Back to Setup</button>
											&nbsp;
											<button class="btn btn-primary" data-integrapay="SubmitButton" type="<?php echo UsingJavaScriptPayments() ? 'button' : 'submit'; ?>">Process Payment</button>
										</div>
									</div>
								</form>
<?php
							}
							else // $xmlDoc->result != 'OK' (i.e. PreHostedRealTimePayment error)...
							{
?>
								<form method="GET" action="<?php echo PageUrl('Setup'); ?>">
									<div class="alert alert-danger">
										<div class="form-group">
											<strong><?php echo $xmlDoc->errortype; ?></strong>
										</div>
										<div class="form-group">
											<?php echo $xmlDoc->errormessage ?>
										</div>
										<div class="form-group">
											<button class="btn btn-danger" type="submit">Back to Setup</button>
										</div>
									</div>
								</form>
<?php
							}
						}
					}
					elseif ($page == 'Result')
					{
						//----------------------------------------------------------------------------------------------------
						// RESULT PAGE
						// 
						// This section represents the page of your website that issues the PostHostedRealTimePayment
						// request to retrieve the result of a payment attempt, and then processes that result.
						//----------------------------------------------------------------------------------------------------

						$webPageToken = $_POST['webPageToken'];
						if (!isset($webPageToken)) // redirects return 'webPageToken' as an HTTP GET variable...
						{
							$webPageToken = $_GET['webPageToken'];
						}

						if (!isset($webPageToken))
						{
?>
							<div class="alert alert-danger">
								<h4>Missing or invalid GET/POST variable(s):</h4>
								<ul>
									<?php if (!isset($webPageToken)) echo '<li>webPageToken</li>'."\r\n"; ?>
								</ul>
							</div>
<?php
						}
						else // required POST variables are set...
						{
							$request = PostHostedRealTimePaymentRequest($username, $password, $webPageToken);
							$response = SendHttpRequest($apiUrl, "POST", $request);
							$xmlDoc = new SimpleXMLElement($response);
							if ($xmlDoc->result == 'OK')
							{
?>
								<form method="GET" action="<?php echo PageUrl('Setup'); ?>">
<?php
									if (isset($_POST['CreditCardName']) && !empty($_POST['CreditCardName']))
									{
?>
										<div class="panel panel-default">
											<div class="panel-heading">Information from JavaScript Payments &lt;form&gt; inputs</div>
											<div class="panel-body">
												<div class="form-group">
													<table class="table table-condensed table-hover">
														<tr>
															<th class="col-sm-2">Field</th>
															<th>Value</th>
														</tr>
														<tr>
															<td>Name on Card</td>
															<td><?php echo $_POST['CreditCardName']; ?></td>
														</tr>
														<tr>
															<td>Card Number</td>
															<td><?php echo $_POST['CreditCardNumber']; ?></td>
														</tr>
														<tr>
															<td>Expiry Month/Year</td>
															<td><?php echo $_POST['CreditCardExpiryMonth'].'/'.$_POST['CreditCardExpiryYear']; ?></td>
														</tr>
														<tr>
															<td>CCV</td>
															<td><?php echo $_POST['CreditCardSecurityCode']; ?></td>
														</tr>
														<tr>
															<td>Sample Input 1</td>
															<td><?php echo $_POST['sampleInput1']; ?></td>
														</tr>
														<tr>
															<td>Sample Input 2</td>
															<td><?php echo $_POST['sampleInput2']; ?></td>
														</tr>
													</table>
												</div>
											</div>
										</div>
<?php
									}
?>
									<div class="panel panel-default">
										<div class="panel-heading">PostHostedRealTimePayment Response</div>
										<div class="panel-body">
											<div class="form-group">
												<table class="table table-condensed table-hover">
													<tr>
														<th class="col-sm-2">Field</th>
														<th>Value</th>
													</tr>
<?php
													foreach ($xmlDoc->children() as $key=> $node)
													{
?>
														<tr>
															<td><?php echo $key; ?></td>
															<td><?php echo $node[0]; ?></td>
														</tr>
<?php
													}
?>
												</table>
											</div>
										</div>
									</div>
									<div class="form-group">
										<button class="btn btn-default" type="submit">Back to Setup</button>
									</div>
								</form>
<?php
							}
							else // $xmlDoc->result != 'OK' (i.e. PostHostedRealTimePayment error)...
							{
?>
								<form method="GET" action="<?php echo PageUrl('Setup'); ?>">
									<div class="alert alert-danger">
										<div class="form-group">
											<strong><?php echo $xmlDoc->errortype; ?></strong>
										</div>
										<div class="form-group">
											<?php echo $xmlDoc->errormessage ?>
										</div>
										<div class="form-group">
											<button class="btn btn-danger" type="submit">Back to Setup</button>
										</div>
									</div>
								</form>
<?php
							}
						}
					}
					else // invalid $_GET['page'] value...
					{
						//----------------------------------------------------------------------------------------------------
						// This section displays an error for $_GET['page'] values that are invalid for this specific
						// sample PHP script, and therefore does not directly represent a page of your website.
						//----------------------------------------------------------------------------------------------------
?>
						<form method="GET" action="<?php echo PageUrl('Setup'); ?>">
							<div class="alert alert-danger">
								<h4>Invalid 'page' variable</h4>
								<div class="form-group">
									<strong>If specified, the value of 'page' must be one of the following:</strong>
								</div>
								<div class="form-group">
									<ul>
										<li>Setup</li>
										<li>Payment</li>
										<li>Result</li>
									</ul>
								</div>
								<div class="form-group">
									<button type="submit" class="btn btn-danger">Back to Setup</button>
								</div>
							</div>
						</form>
<?php
					}
				}
?>
			</div>
		</body>
<?php
	}

	function IsLocalUrl($url)
	{
		return strpos(strtolower($url), 'localhost') !== false || strpos($url, '127.0.0.1') !== false;
	}

	function PageUrl($page)
	{
		return (isset($_SERVER['HTTPS']) ? "https" : "http").'://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'].'?page='.$page;
	}

	function PostHostedRealTimePaymentRequest($username, $password, $webPageToken)
	{
		return
			'<request>'
				.'<username>'.$username.'</username>'
				.'<password>'.$password.'</password>'
				.'<command>PostHostedRealTimePayment</command>'
				.'<webPageToken>'.$webPageToken.'</webPageToken>'
			.'</request>';
	}

	function PreHostedRealTimePaymentRequest($username, $password, $homeUrl)
	{
		$returnUrl = PageUrl("Result");
		if (IsLocalUrl($returnUrl))
		{
			$returnUrl = $homeUrl;
		}

		return
			'<request>'
				.'<username>'.$username.'</username>'
				.'<password>'.$password.'</password>'
				.'<command>PreHostedRealTimePayment</command>'
				.(!UsingJavaScriptPayments() ? '<returnUrl>'.$returnUrl.'</returnUrl>' : '')
				.XmlFromPost('transactionID')
				.XmlFromPost('transactionAmountInCents')
				.XmlFromPost('transactionDescription')
				.XmlFromPost('payerUniqueID')
				.XmlFromPost('payerFirstName')
				.XmlFromPost('payerLastName')
				.XmlFromPost('payerAddressStreet')
				.XmlFromPost('payerAddressSuburb')
				.XmlFromPost('payerAddressState')
				.XmlFromPost('payerAddressPostCode')
				.XmlFromPost('payerAddressCountry')
				.XmlFromPost('payerEmail')
				.XmlFromPost('payerPhone')
				.XmlFromPost('payerMobile')
				.(UsingTransparentRedirect() ? '<transparentRedirect>1</transparentRedirect>' : '')
				.(UsingJavaScriptPayments() ? '<javascriptSubmit>1</javascriptSubmit>' : '')
				.XmlFromPost('auditUsername')
			.'</request>';
	}

    function SendHttpRequest($url, $method = 'GET', $content = '', $contentType = 'text/xml')
    {
        $options = array(
	        'http' => array(
		        'method' => $method,
		        'header' => 'Content-Type: '.addslashes($contentType).'\r\n'
						        .'Content-Length: '.strlen($content).'\r\n',
		        'content' => $content
	        )
        );

        $context = stream_context_create($options);

        return file_get_contents($url, false, $context);
    }

	function UsingHostedPageRedirect()
	{
		return $_POST['flow'] == 'HostedPageRedirect';
	}

	function UsingJavaScriptPayments()
	{
		return $_POST['flow'] == 'JavascriptPayments' ||
			!(UsingHostedPageRedirect() || UsingTransparentRedirect());
	}

	function UsingTransparentRedirect()
	{
		return $_POST['flow'] == 'TransparentRedirect';
	}

	function WriteMonthOptions($indent = 24)
	{
		$currentMonth = date('n');
		for ($i = 0; $i < 12; $i++)
		{
			if ($i > 0)
			{
				echo str_repeat(' ', $indent);
			}

			echo '<option value="'.sprintf("%02d", $i + 1).'"';
			if ($i + 1 == $currentMonth)
			{
				echo ' selected';
			}

			echo '>'.date('F', strtotime("+$i months", 0)).'</option>'."\r\n";
		}
	}

	function WriteYearOptions($indent = 24)
	{
		$currentYear = date('Y');
		for ($i = 0; $i < 5; $i++)
		{
			if ($i > 0)
			{
				echo str_repeat(' ', $indent);
			}

			echo '<option value="'.($currentYear + $i).'">'.($currentYear + $i).'</option>'."\r\n";
		}
	}

	function XmlFromPost($name)
	{
		if (!isset($_POST[$name]) || empty($_POST[$name]))
		{
			return ''; // missing or empty POST variable
		}

		return '<'.$name.'>'.$_POST[$name].'</'.$name.'>';
	}
?>
</html>
